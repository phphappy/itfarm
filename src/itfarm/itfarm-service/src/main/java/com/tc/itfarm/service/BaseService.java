package com.tc.itfarm.service;

import com.tc.itfarm.api.exception.BusinessException;

import java.util.List;

/**
 * Created by Administrator on 2016/8/14.
 */
public interface BaseService<T> {

    Integer insert(T entity);

    Integer update(T entity);

    Integer updateBySelective(T entity);

    Integer delete(Integer id);

    T select(Integer id);

    List<T> selectAll();
}
