package com.tc;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;


/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws IOException {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("dubbo.xml");
        System.out.println("----------------------------------");
        System.out.println("---------dubbo run success--------");
        System.out.println("----------------------------------");
        System.in.read();
    }
}
