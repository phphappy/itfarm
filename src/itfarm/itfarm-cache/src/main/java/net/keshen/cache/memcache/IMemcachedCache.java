/*
 * @Project: itfarm-cache
 * @Author shenke
 * @(#)IMemcachedCache.java  Created on 2016年7月1日
 * @Copyright (c) 2016 ZDSoft Inc. All rights reserved
 * @Date 2016年7月1日
 */
package net.keshen.cache.memcache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @description: memcache cache
 * @author shenke
 * @version Revision: 1.0 , 
 * @date: 2016年7月1日 上午9:31:06
 */
public class IMemcachedCache {
	
		private static final Logger log = LoggerFactory.getLogger(IMemcachedCache.class);
}
